const express = require('express');

const socketIO = require('socket.io');

const http = require('http');

const path = require('path');
const {Usuarios} = require('./classes/usuario');

const app = express();

let server = http.createServer(app);

const publicPath = path.resolve(__dirname, '../public');
const port = process.env.PORT || 3000;

app.use(express.static(publicPath));

//IO = esta es la comunicacion del backend
let io = socketIO(server);

const usuarios = new Usuarios();

const {crearMensaje} = require('./utilidades/utilidades');

io.on('connection', (client) => {
  console.log('Usuario conectado');

  client.on('entrarChat', (data, cb) => {
    console.log('Usuario conectado al CHAT', data.nombre);
    if (!data.nombre || !data.sala) {
      return cb({
        error: true,
        mensaje: 'Elnombre es necesario',
      });
    }

    client.join(data.sala);

    let personas = usuarios.agregarPersona(client.id, data.nombre, data.sala);

    //cb(personas);
    console.log(usuarios.getPersonas());

    client.broadcast
      .to(data.sala)
      .emit('listaPersonas', usuarios.getPersonas());
    /* io.sockets.emit('listaPersonas', usuarios.getPersonas());
   return cb({
      personas,
    }); */
    cb(usuarios.getPersonasPorSala(data.sala));
  });

  client.on('crearMensaje', (data) => {
    let persona = usuarios.getPersona(client.id);
    let mensaje = crearMensaje(persona.nombre, data.mensaje);
    client.broadcast.emit('crearMensaje', mensaje);
  });

  client.on('chat:message', (data) => {
    let persona = usuarios.getPersona(client.id);
    let mensaje = crearMensaje(persona.nombre, data.mensaje);
    //Envia solo al cliente que lo emite
    //client.broadcast.emit('chat:message', mensaje);
    //Enviar a todos lo miembros del sockets
    io.sockets.emit('chat:message', mensaje);
  });
  client.on('chat:typing', (data) => {
    client.broadcast.emit('chat:typing', data);
  });

  client.on('disconnect', () => {
    let personaBorrada = usuarios.borrarPersona(client.id);

    client.broadcast.emit('crearMensaje', {
      usuario: 'SERVIDOR',
      mensaje: crearMensaje(
        'SERVIDOR',
        `${personaBorrada.nombre} salio del CHAT`
      ),
    });

    client.broadcast.emit('listaPersonas', usuarios.getPersonas());
  });

  client.on('mensajePrivado', (data) => {
    let persona = usuarios.getPersona(client.id);

    client.broadcast
      .to(data.para)
      .emit('mensajePrivado', crearMensaje(persona.nombre, data.mensaje));
  });
});

server.listen(port, (err) => {
  if (err) throw new Error(err);

  console.log(`Servidor corriendo en puerto ${port}`);
});
