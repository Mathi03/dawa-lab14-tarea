var socket = io();

//ELEMENTS

let message = document.getElementById('message');
let username = document.getElementById('username');
let btn = document.getElementById('send');
let output = document.getElementById('output');
let actions = document.getElementById('actions');
let userWindow = document.getElementById('user-window');

var params = new URLSearchParams(window.location.search);

if (!params.has('nombre')) {
  window.location = 'index.html';
  throw new Error('El nombre es necesario');
}

var usuario = {
  nombre: params.get('nombre'),
  sala: params.get('sala'),
};
username.value = usuario.nombre;

socket.on('connect', function () {
  console.log('Conectando al servidor');

  socket.emit('entrarChat', usuario, function (resp) {
    console.log('Usuarios conectados', resp);
    renderizarUsuarios(resp);
  });
});

socket.on('disconnect', function () {
  console.log('Perdimos conexion con el servidor');
});

socket.on('crearMensaje', function (data) {
  console.log('Servidor mensaje: ', data);
});

socket.on('chat:message', function (data) {
  //console.log(data);
  actions.innerHTML = '';
  output.innerHTML += `<p>
    <strong>${data.nombre}: </strong>${data.mensaje}
  </p>`;
});

socket.on('listaPersonas', function (personas) {
  renderizarUsuarios(personas);
});

function renderizarUsuarios(personas) {
  console.log(personas);
  let arregloUsuarios = [];
  personas.map((persona) => {
    console.log(persona.nombre);
    arregloUsuarios.push(persona.nombre);
    userWindow.innerHTML = '';
  });
  function agregar(arreglo) {
    for (let index = 0; index < arreglo.length; index++) {
      userWindow.innerHTML += `<p>${arreglo[index]}</p>`;
    }
  }
  agregar(arregloUsuarios);
}

btn.addEventListener('click', function () {
  socket.emit('chat:message', {
    mensaje: message.value,
  });
  message.value = '';
});

message.addEventListener('keypress', function () {
  socket.emit('chat:typing', usuario.nombre);
});

socket.on('chat:typing', function (data) {
  actions.innerHTML = `<p><em>${data} esta escribiendo...</em></p>`;
});
socket.on('mensajePrivado', function (data) {
  console.log(data);
});
